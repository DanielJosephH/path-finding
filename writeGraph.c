#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "structures.h"
#include "dijkstraStructures.h"
#include "buildGraph.h"
#include "traverseGraph.h"


void writeOut(Graph *graph) {

  FILE *fp = fopen("graph.txt", "w");
  if(!fp) {
    printf("ERROR: file could not be opened or created!\n");
    exit(0);
  }
  fprintf(fp, "%s\n", "\"Map\"");

  Node *node = graph->head;
  for(int i = 0; i < graph->size; i++) {
    AdjNode *edge = node->head;
    for(int q = 0; q < node->size; q++) {

      if(node->id < edge->id) {
        fprintf(fp, "%lf %lf\n", node->x, node->y);
        Node *adjNode = getNode(graph, edge->id);
        fprintf(fp, "%lf %lf\n\n", adjNode->x, adjNode->y);
      }
      edge = edge->next;
    }
    node = node->next;
  }



  fclose(fp);
}

void writePath(Graph *graph, aNodeList *list, bool append) {

  FILE *fp2 = NULL;
  if(!append) {
    fp2 = fopen("path.txt", "w");
  } else {
    fp2 = fopen("path.txt", "a");
  }
  if(!fp2) {
    printf("ERROR: file pointer is NULL, cannot write to file\n");
    exit(0);
  }
  if(!append) {
    fprintf(fp2, "%s\n", "\"Route\"");
  } else {
    fprintf(fp2, "\n\n%s\n", "\"Route 2\"");
  }

  aNode *node = list->current;
  int i = 0; // the i counter is used to prevent an incorrectly formatted path going into an infinite loop.
  while(node->precursorId != node->id && i < list->size) {

    Node *curNode = getNode(graph, node->id);
    fprintf(fp2, "%lf %lf\n", curNode->x, curNode->y);

    Node *prevNode = getNode(graph, node->precursorId);
    fprintf(fp2, "%lf %lf\n\n", prevNode->x, prevNode->y);

    node = getANode(list, node->precursorId);
    if(node == NULL) {
      printf("error!\n");
    }
    i++;
  }

  fclose(fp2);
}
