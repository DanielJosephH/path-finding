#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "structures.h"

#define DEBUG false

Node * getNode(Graph *graph, int id) {
  // test each node id, if the ids do not match then test the next node
  // if no node found, return NULL.
  Node *node = graph->head;
  while(node) {
    if(node->id == id) {
      return node;
    }
    node = node->next;
  }
  return NULL;
}

AdjNode * getAdjNode(Node *node, int id) {
  // head should be the known head node if the AdjNode, this can be obtained using the getNode function
  AdjNode *current = node->head;
  while(current) {
    if(current->id == id) {
      return current;

    }
    current = current->next;
  }
  return NULL;
}

void createAdjNode(Node *node, int id, double distance) {

  AdjNode *edge = (AdjNode *)malloc(sizeof(AdjNode));
  edge->id = id;
  edge->distance = distance;
  edge->next = NULL;

  if(!node->head) {
    node->head = edge;
  } else {
    AdjNode *current = node->head;

    while(current->next) {
      current = current->next;
    }
    current->next = edge;
  }
    node->size++;
}

void createNode(Graph *graph, int id, double x, double y) {

  // an x and y position of -99 signifies that the xy is not set.
  Node *node = (Node *)malloc(sizeof(Node));
  if(node == NULL) {
    printf("ERROR: during node pointer memory allocation");
  }

  node->id = id;
  node->x = x;
  node->y = y;
  node->next = NULL;
  node->head = NULL;
  node->size = 0;


  int i = graph->size;
  if(i == 0) {
    // node is the head node
    graph->head = node;
  } else {
    Node *parent = graph->head;
    while(parent->next) {
      // search through path until next == NULL node is found
      parent = parent->next;
    }
    parent->next = node;
  }
  graph->size++;
}

Graph * createGraph() {

    FILE *file = fopen("./Final_Map.map", "r");
    if(!file) {
      printf("ERROR: file pointer is NULL, could not load file\n");
      exit(0);
    }

    bool shouldScan = true;
    char line[100], *values[10];

    // allocate memory for the graph and all of its flexible components
    Graph *graph = (Graph *)malloc(sizeof(Graph));
    graph->head = NULL;
    graph->size = 0;
    if(graph == NULL) {
      printf("ERROR: graph pointer is NULL");
      exit(0);
    }

    while(shouldScan) {

      fgets(line, 100, file);

      // check if links or nodes are being read, if all links/nodes have been read break from the loop
      char *lineTok = strtok(line, " ");

      // segment the line into values.
      int i = 0;
      while(lineTok != NULL) {
          lineTok = strtok(NULL, " ");
          values[i] = lineTok;
          i++;
      }

      // remove the unneeded 'definition' in the value.
      char *attribute, *value;
      for(int i = 0; i < 5; i++) {
        attribute = strtok(values[i], "=");
        value = strtok(NULL, "=");
        values[i] = value;
      }

      // the line encodes for a link.
      if(!strcmp(line, "<link")) {

        int nodeid1, nodeid2;
        double distance;

        // save the values of the link.
        sscanf(values[1], "%d", &nodeid1);
        sscanf(values[2], "%d", &nodeid2);
        sscanf(values[4], "%lf", &distance);

        // check if the nodes already exist, if they do then add the adjacent node, if not then create the node(s) first.
        if(!getNode(graph, nodeid1)) {
          createNode(graph, nodeid1, -99, -99);
        }

        createAdjNode(getNode(graph, nodeid1), nodeid2, distance);

        if(!getNode(graph, nodeid2)) {
          createNode(graph, nodeid2, -99, -99);
        }

        createAdjNode(getNode(graph, nodeid2), nodeid1, distance);

      } else if(!strcmp(line, "<node")) {
        // line encodes for a node.

        int nodeid;
        double x, y;

        // set the values of the node and add it to the graph.
        sscanf(values[0], "%d", &nodeid);
        sscanf(values[1], "%lf", &x);
        sscanf(values[2], "%lf", &y);

        // check if node already exists, as some isolated nodes may exist, there is no point adding in isolated nodes so skip them.
        if(getNode(graph, nodeid)) {
          // set the x and y of an existant node
          getNode(graph, nodeid)->x = x;
          getNode(graph, nodeid)->y = y;
        }
      } else if(!strcmp(line, "<way")) {

        if(DEBUG) {

          Node *node = graph->head;
          for(int i = 0; i < graph->size; i++) {
            printf("\n%d (%lf,%lf)|| ", node->id, node->x, node->y);
              AdjNode *edge = node->head;
              for(int q = 0; q < node->size; q++) {
                printf("(%d, %lf) ", edge->id, edge->distance);
                edge = edge->next;
              }
            if(node->next) {
              node = node->next;
            }
          }
        }


        // all nodes/ links have been read, terminate loop.
        shouldScan = false;
      }

    }

    fclose(file);

    return graph;
}
