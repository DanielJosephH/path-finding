typedef struct aNode_t {
  double distance;
  int id;
  int precursorId;
  int visited; // 0 represents not visited, 1 = visited.
  struct aNode_t *next;
} aNode;

typedef struct aNodeList_t {
  int size;
  aNode *current;
  aNode *head;
} aNodeList;
