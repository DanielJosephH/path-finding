#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "structures.h"
#include "buildGraph.h"
#include "dijkstraStructures.h"
#include "writeGraph.h"
#include "destroyGraph.h"

aNode * getANode(aNodeList *list, int id) {

  aNode *node = list->head;
  for(int i = 0; i < list->size; i++) {
    if(node->id == id) {

      return node;
    }
    node = node->next;
  }

  return NULL;
}

aNodeList * createUnvisitedList(Graph *graph, Node *start) {

  aNodeList *unvList = (aNodeList *)malloc(sizeof(aNodeList));
  if(!unvList) {
    printf("ERROR: aNodeList pointer is null\n");
    exit(0);
  }
  unvList->size = 0;
  unvList->head = NULL;

  Node *node = graph->head;
  aNode *previous = NULL;
  for(int i = 0; i < graph->size; i++) {

    aNode *anode = (aNode *)malloc(sizeof(aNode));
    if(!anode) {
      printf("ERROR: anode pointer is null\n");
      exit(0);
    }
    anode->id = node->id;
    anode->next = NULL;

    if(anode->id == start->id) {

      // this is the start node, set its distance to 0.
      anode->distance = 0;
      unvList->current = anode;
      anode->precursorId = anode->id;

    } else {

      // set the distance of all other nodes to 'infinity' (-99).
      anode->distance = -99;

    }

    anode->visited = 0;

    if(!unvList->head) {
      unvList->head = anode;
    } else {
      previous->next = anode;
    }
    previous = anode;
    unvList->size++;

    node = node->next;
  }

  return unvList;
}

aNodeList * dijkstra(Graph *graph, int startId, int endId) {

    Node *start = getNode(graph, startId);
    if(!start) {
      printf("ERROR: no such node found with id: %d\n", startId);
      exit(0);
    }

    if(!getNode(graph, endId)) {
      printf("ERROR: no such node found with id: %d\n", endId);
      exit(0);
    }
    aNodeList *unvList = createUnvisitedList(graph, start);

    int loops = 0;

    while(unvList->current->id != endId && loops < unvList->size) {

      // expand the current node
      Node *node = getNode(graph, unvList->current->id);
      AdjNode *adj = node->head;
      aNode *anode = NULL;
      for(int i = 0; i < node->size; i++) {
        anode = getANode(unvList, adj->id);
        if(anode->visited != 1) {

          // check if the tentative distance is less than the current distance, -99 means no distance set yet
          double distance = unvList->current->distance + adj->distance;
          if(distance < anode->distance || anode->distance == -99) {

            // set the new distance to the node, as it is smaller than previous
            anode->precursorId = unvList->current->id;
            anode->distance = distance;

          }
        }
        adj = adj->next;
      }

        // set the current node as visited
        unvList->current->visited = 1;

        // set the new current node
        double minDist = -1;
        int currentId;
        aNode *anode2 = unvList->head;
        for(int q = 0; q < unvList->size; q++) {
          if((anode2->distance < minDist || minDist == -1) && anode2->visited == 0 && anode2->distance != -99) {
            minDist = anode2->distance;
            currentId = anode2->id;

          }
          anode2 = anode2->next;
        }
        if(minDist != -1) {
            unvList->current = getANode(unvList, currentId);
        }
        loops++;
    }

    if(unvList->current->id == endId) {
      return unvList;
    } else {
      free(unvList);
      return NULL;
    }
}


void algorithm(Graph *graph, int startId, int endId) {

  aNodeList *list = dijkstra(graph, startId, endId);
  if(!list) {
    printf("ERROR: no route found!\n");
  } else {

    // write out the results of the algorithm
    writePath(graph, list, false);
    destroyAlgorithmList(list->head);
    free(list);
  }
}

void minRouteWPoint(Graph *graph, int startId, int endId, int visitId) {
  // path will be calculated by conjugating the route from startId to visitId with the route from
  // visitId to endId
  aNodeList *path1 = dijkstra(graph, startId, visitId);
  writePath(graph, path1, false);
  aNodeList *path2 = dijkstra(graph, visitId, endId);
  writePath(graph, path2, true);

  // destroyAlgorithmList(list->head);
  // free(list);
}

void scenicRoute(Graph *graph, int startId, int endId) {

}
