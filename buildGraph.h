Node *getNode(Graph *graph, int id);
AdjNode * getAdjNode(Node *node, int id);
void createAdjNode(Node *node, int id, double distance);
void createNode(Graph *graph, int id, double x, double y);
Graph *createGraph();
