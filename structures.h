typedef struct adjNode_t {
  int id;
  double distance;
  struct adjNode_t *next;
} AdjNode;

typedef struct node_t {
  int id;
  int size;
  double x;
  double y;
  AdjNode *head;
  struct node_t *next;
} Node;

typedef struct graph_t {
  // size is the number of adjacency lists, i.e. number of nodes.
  int size;
  Node *head;
} Graph;
