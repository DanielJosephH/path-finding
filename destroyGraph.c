#include <stdio.h>
#include <stdlib.h>

#include "structures.h"
#include "dijkstraStructures.h"

void destroyEdge(AdjNode *edge) {

  if(!edge) {

  } else if(!edge->next) {
    free(edge);
  } else {
    destroyEdge(edge->next);
    free(edge);
  }
}

void destroyGraph(Node *node) {

    if(!node) {

    } else if(!node->next) {
      destroyEdge(node->head);
      free(node);
    } else {
      destroyGraph(node->next);
      destroyEdge(node->head);
      free(node);
    }

}

void destroyAlgorithmList(aNode *node) {

  if(!node) {

  } else if(!node->next) {
    free(node);
  } else {
    destroyAlgorithmList(node->next);
    free(node);
  }
}
