
# code details

EXE_DIR = .
EXE = $(EXE_DIR)/calculatePath

SRC= main.c buildGraph.c writeGraph.c destroyGraph.c traverseGraph.c
OUT= graph.txt path.txt

# generic build details

CC=      gcc
COPT=    -g
CFLAGS= -lm

# compile to  object code

OBJ= $(SRC:.c=.o)

.c.o:
	$(CC) $(COPT) -c -o $@ $< -std=c99

# build executable

$(EXE): $(OBJ)
	$(CC) $(OBJ) $(CFLAGS) -o $(EXE)

# clean up compilation

clean:
	rm -f $(OBJ) $(EXE) $(OUT)

# display results of program

run:
	rm -f graph.txt path.txt
	./calculatePath

display:
	gnuplot -persist graph.gnu

# dependencies

main.o: main.c buildGraph.h structures.h writeGraph.h traverseGraph.h destroyGraph.h
buildGraph.o: buildGraph.c structures.h
writeGraph.o: writeGraph.c structures.h buildGraph.h dijkstraStructures.h traverseGraph.h
traverseGraph.o: traverseGraph.c structures.h buildGraph.h dijkstraStructures.h writeGraph.h destroyGraph.h
destroyGraph.o: destroyGraph.c structures.h dijkstraStructures.h
