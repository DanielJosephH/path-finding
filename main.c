#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#include "structures.h"
#include "buildGraph.h"
#include "dijkstraStructures.h"
#include "writeGraph.h"
#include "traverseGraph.h"
#include "destroyGraph.h"

int main(int argc, char const *argv[]) {

  Graph *graph = createGraph();

  printf("\n\nPlease select a route type to calculate:\n"
    "1.Shortest path\n"
    "2.Shortest path passing through specified point\n");

  bool shouldRun = true;
  while(shouldRun) {

    char input[20];
    int choice, nodeId1 = -1, nodeId2 = -1, nodeId3 = -1;
    printf("\n\n>>>>\n");

    fgets(input, 20, stdin);
    sscanf(input, "%d", &choice);

    if (choice == 1) {

      printf("\nPlease enter two node id's in the form: x y\n>>>>\n");
      fgets(input, 20, stdin);
      sscanf(input, "%d %d", &nodeId1, &nodeId2);

      algorithm(graph, nodeId1, nodeId2);

      shouldRun = false;

    } else if (choice == 2) {

      printf("\nPlease enter three node id's in the form: x y z\n"
      "where x and y are end points and z is the point to visit\n>>>>\n");
      fgets(input, 20, stdin);
      sscanf(input, "%d %d %d", &nodeId1, &nodeId2, &nodeId3);

      minRouteWPoint(graph, nodeId1, nodeId2, nodeId3);

      shouldRun = false;

    } else {

      printf("\nPlease enter a valid route type id!\n");
    }

  }

  writeOut(graph);

  destroyGraph(graph->head);
  free(graph);

  return 0;
}
