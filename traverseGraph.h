aNode * getANode(aNodeList *list, int id);
aNodeList * createUnvisitedList(Graph *graph, Node *start);
aNodeList * dijkstra(Graph *graph, int startId, int endId);
void algorithm(Graph *graph, int startId, int endId);
void minRouteWPoint(Graph *graph, int startId, int endId, int visitId);
